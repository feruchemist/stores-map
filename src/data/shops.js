const places = {
    "features": [
        {
            "id": 1,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "проспект Добровольського, 122 К1, Одеса"
            },
            "geometry": {
                "coordinates": [30.798447595402013, 46.59180336416647],
                "type": "Point"
            }
        },
        {
            "id": 2,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "Миколаївська дорога, 170a, Одеса"
            },
            "geometry": {
                "coordinates": [30.76241049278372, 46.55318410940655],
                "type": "Point"
            }
        },
        {
            "id": 3,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вулиця Балківська, 22 а, Одеса"
            },
            "geometry": {
                "coordinates": [30.71011247378189, 46.48773098755001],
                "type": "Point"
            }
        },
        {
            "id": 4,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вулиця Розумовська, 10/12, Одеса"
            },
            "geometry": {
                "coordinates": [30.720455461223484, 46.47417825118849],
                "type": "Point"
            }
        },
        {
            "id": 5,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вул. Катерининська, 90, прим, 101, Одеса"
            },
            "geometry": {
                "coordinates": [30.736591450611296, 46.47019623479691],
                "type": "Point"
            }
        },
        {
            "id": 6,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вулиця Балківська, 199, Одеса"
            },
            "geometry": {
                "coordinates": [30.70721601137392, 46.46179557567316],
                "type": "Point"
            }
        },
        {
            "id": 7,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вулиця Іцхака Рабіна, 2/1, Одеса"
            },
            "geometry": {
                "coordinates": [30.705300580095848, 46.44389626703603],
                "type": "Point"
            }
        },
        {
            "id": 8,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вулиця Маршала Говорова, 8, Одеса"
            },
            "geometry": {
                "coordinates": [30.74621925299606, 46.44693955853321],
                "type": "Point"
            }
        },
        {
            "id": 9,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вулиця Інглезі, 6-в, Одеса"
            },
            "geometry": {
                "coordinates": [30.72146297297721, 46.416566629680034],
                "type": "Point"
            }
        },
        {
            "id": 10,
            "type": "Feature",
            "properties": {
                "entity": "shop",
                "title": "ATB market",
                "description": "вулиця Ільфа та Петрова, 10, Одеса"
            },
            "geometry": {
                "coordinates": [30.713719955687782, 46.40085655389252],
                "type": "Point"
            }
        },
        {
            "id": 11,
            "type": "Feature",
            "properties": {
                "entity": "mall",
                "title": "ТЦ Европа на Дерибасовской",
                "description": "вулиця Дерибасівська, 21, Одеса"
            },
            "geometry": {
                "coordinates": [30.737693356357575, 46.48392491981695],
                "type": "Point"
            }
        },
        {
            "id": 12,
            "type": "Feature",
            "properties": {
                "entity": "mall",
                "title": "ЦУМ",
                "description": "вулиця Пушкінська, 72, Одеса"
            },
            "geometry": {
                "coordinates": [30.74062706803578, 46.47082411963125],
                "type": "Point"
            }
        },
        {
            "id": 13,
            "type": "Feature",
            "properties": {
                "entity": "mall",
                "title": "City Center",
                "description": "ТЦ Сити Центр, проспект Небесної Сотні, 2, Одеса"
            },
            "geometry": {
                "coordinates": [30.711003321389207, 46.416215527535506],
                "type": "Point"
            }
        },
        {
            "id": 14,
            "type": "Feature",
            "properties": {
                "entity": "mall",
                "title": "Сады Победы",
                "description": "вулиця Академічна, 28, Одеса"
            },
            "geometry": {
                "coordinates": [30.758126238286053, 46.44020412478693],
                "type": "Point"
            }
        },
        {
            "id": 15,
            "type": "Feature",
            "properties": {
                "entity": "mall",
                "title": "Лаванда",
                "description": "вулиця Іцхака Рабіна, 2, Одеса"
            },
            "geometry": {
                "coordinates": [30.705322978766375, 46.444581671086105],
                "type": "Point"
            }
        },
    ],
    "type": "FeatureCollection"
}

export default places;

