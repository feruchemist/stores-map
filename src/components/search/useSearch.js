import { useState } from "react";
import { fetchSuggestions } from "./utils";


export const useSearch = (initialValue) => {
    const [suggestions, setSuggestions] = useState([]);
    const [value, setValue] = useState(initialValue);
    const [query, setQuery] = useState(initialValue);

    const handleChange = async (event) => {
        setQuery(event.target.value);
        const options = await fetchSuggestions(event.target.value);

        setSuggestions(options?.features || []);
    };

    return {
        value,
        query,
        onChange: handleChange,
        setValue,
        setQuery,
        suggestions,
        setSuggestions
    };
}
