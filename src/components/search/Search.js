import React from "react";
import './_styles.css';

export const Search = ({address}) => {
    const {
        query,
        value,
        onChange,
        setValue,
        setQuery,
        suggestions,
        setSuggestions
    } = address;

    const handleClearSearch = () => {
        setValue("");
        setQuery("");
        setSuggestions([]);
    }

    return (
        <div className="address-field _mb-15">
            <input
                name="address"
                placeholder="Address"
                value={query}
                onChange={onChange}
                autoComplete="off"
            />
            {(query || value) &&
                <div className="address-field-clear" onClick={handleClearSearch}>&times;</div>
            }
            {suggestions?.length > 0 && (
                <div className="address-field-results">
                    {suggestions.map((suggestion, index) => {
                        return (
                            <div className="address-field-result"
                                key={index}
                                onClick={() => {
                                    setValue(suggestion.place_name);
                                    setQuery(suggestion.place_name);
                                    setSuggestions([]);
                                }}
                            >
                                {suggestion.place_name}
                            </div>
                        );
                    })}
                </div>
            )}
        </div>
    );
};