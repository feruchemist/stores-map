import { mapToken } from "../map/constants";

export const fetchSuggestions = async (v) => {
    try {
        const endpoint = `https://api.mapbox.com/geocoding/v5/mapbox.places/${v}.json?access_token=${mapToken}&autocomplete=true`;
        const response = await fetch(endpoint);
        return await response.json();
    } catch (error) {
        console.log("Error fetching data, ", error);
    }
}