import { mapToken } from "./constants";

export const getEntities = (data) => {
    const entities = [];
    data.forEach(item => entities.indexOf(item.properties.entity) === -1 && entities.push(item.properties.entity))
    return entities
}

export const filterEntities = (data, entity) => {
    if (entity === 'all') return data;

    return {
        "type": "FeatureCollection",
        "features": data.features.filter(shop => shop.properties.entity === entity)
    }
}

export const getCoordsByAddress = async (address) => {
    try {
        const endpoint = `https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?access_token=${mapToken}`;
        const response = await fetch(endpoint);
        return await response.json();
    } catch (error) {
        console.log("Error fetching data, ", error);
    }
}

export const renderEntityPopup = (title, description) => {
    return `<div class="_py-15"><h3>${title}</h3><div>${description}</div></div>`
}