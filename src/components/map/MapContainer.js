import React, {useState, useMemo, useEffect, useCallback} from "react";
import {filterEntities, getCoordsByAddress, getEntities} from "./utils";
import {Tabs} from "../tabs/Tabs";
import {EntitiesList} from "../entities/EntitiesList";
import {Search} from "../search/Search";
import {useSearch} from "../search/useSearch";
import {useMap} from "./useMap";
import "mapbox-gl/dist/mapbox-gl.css";
import "./_style.css"
import shops from "../../data/shops.js";
import {Affix} from "../affix/Affix";


export const MapContainer = () => {
    const tabs = ['all', ...getEntities(shops.features)];

    const address = useSearch("");
    const [activeTab, setActiveTab] = useState('all');

    const features = useMemo(() => filterEntities(shops, activeTab), [activeTab]);

    const {mapContainer, setZoom, setCenter, showEntityPopup} =
        useMap([30.73237784726677, 46.481505501473514], 9, features);

    const handleTabChange = (tab) => {
        setActiveTab(tab);
        setZoom(9);
    }

    const handleCenterChange = useCallback(center => {
        setCenter(center);
        setZoom(12);
    }, [setCenter, setZoom]);

    const handleClickOnEntity = entity => {
        handleCenterChange(entity.geometry.coordinates);
        showEntityPopup(entity)
    };

    useEffect(() => {
        async function updateMap() {
            const coords = await getCoordsByAddress(address.value);
            if (coords?.features?.[0]) {
                handleCenterChange(coords?.features?.[0].center);
            }
        }

        if (address.value) {
            updateMap()
        }
    }, [address.value, handleCenterChange])

    return (
        <div className="map-wrap _f _px-15">
            <div className="_w-25 _pr-15">
                <Tabs tabs={tabs} activeTab={activeTab} onChange={handleTabChange}/>
                <EntitiesList entities={features.features} handleClick={handleClickOnEntity}/>
            </div>
            <div className="_w-75">
                <Affix offset={80}>
                    <Search address={address}/>
                    <div ref={mapContainer} className="map-container"/>
                </Affix>
            </div>
        </div>
    )
}