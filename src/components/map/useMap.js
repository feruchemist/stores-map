import {useCallback, useEffect, useRef, useState} from "react";
import mapboxgl from "mapbox-gl";
import {mapToken, markerStyles} from "./constants";
import {renderEntityPopup} from "./utils";

mapboxgl.accessToken = mapToken;

export const useMap = (initialCenter, initialZoom, features) => {
    const mapContainer = useRef(null);
    const map = useRef(null);

    const [markers, setMarkers] = useState([]);
    const [center, setCenter] = useState(initialCenter);
    const [zoom, setZoom] = useState(initialZoom);

    const showEntityPopup = useCallback(entity => {
        markers.forEach(m => m.marker.getPopup().remove());
        const popup = markers.find(m => m.id === entity.id).marker.getPopup();
        popup.addTo(map.current);
    }, [markers])

    useEffect(() => {
        map.current = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/streets-v9',
            center,
            zoom
        });

        map.current.addControl(
            new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            })
        );

        map.current.once("load", () => {
            map.current.addSource("points", {
                type: "geojson",
                data: features
            });
        });

        return () => map.current.remove();
    }, []);

    useEffect(() => {
        if (map.current) {
            map.current.flyTo({center, zoom, speed: 2});
        }
    }, [center, zoom]);

    useEffect(() => {
        if (map.current) {
            markers.forEach(m => m.marker.remove());

            setMarkers(features.features.map(shop => ({
                id: shop.id,
                marker: new mapboxgl.Marker({
                    color: markerStyles[shop.properties.entity] || 'lightblue'
                })
                    .setLngLat(shop.geometry.coordinates)
                    .setPopup(new mapboxgl.Popup()
                        .setHTML(renderEntityPopup(shop.properties.title, shop.properties.description))
                    )
                    .addTo(map.current)
            })))
        }
    }, [features]);


    return {
        mapContainer,
        setZoom,
        setCenter,
        showEntityPopup
    }
}