import React from "react";
import "./_style.css"

export const EntitiesList = ({entities, handleClick}) => {
    return (
        <div className="entities-list">
            {entities.map(entity => (
                <div className="entities-list-item" key={entity.id}>
                    <div className="entity-title">{entity.properties.title}</div>
                    <div className="entity-description"
                         onClick={() => handleClick(entity)}>
                        {entity.properties.description}</div>
                </div>
            ))}
        </div>
    )
}