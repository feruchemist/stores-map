import {useLayoutEffect, useState, useCallback} from "react";

export const Affix = ({offset = 0, children}) => {
    const [stick, setStick] = useState(false);
    const [width, setWidth] = useState("100%");

    const callBackRef = useCallback(domNode => {
        if (domNode) {
            setWidth(domNode.getBoundingClientRect().width);
        }
    }, []);

    const handleScroll = () => {
        setStick(window.scrollY > offset);
    };

    useLayoutEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return (() => {
            window.removeEventListener('scroll', handleScroll);
        });
    });

    return (
        <div ref={callBackRef}>
            <div className={stick ? "sticky" : ""} style={{width}}>
                {children}
            </div>
        </div>
    )
}