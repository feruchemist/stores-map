import React from "react";
import "./_style.css"

export const Tabs = ({tabs, activeTab, onChange}) => {
    return (
        <div className="tabs _f">
            {tabs.map(tab => (
                    <div
                        className={`tab ${activeTab === tab ? "tab-active" : ''}`}
                        onClick={() => onChange(tab)}
                        key={tab}>
                        {tab}
                    </div>
                )
            )}
        </div>
    )
}