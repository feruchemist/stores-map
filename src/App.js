import React from "react";
import { MapContainer } from "./components/map/MapContainer";

function App() {
    return (
        <div className="App _py-15">
            <h1 className="_text-c">Our shops</h1>
            <MapContainer/>
        </div>
    );
}

export default App;
